<?php

namespace Drupal\elevenlabs_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'elevenlabs_default' formatter.
 *
 * @FieldFormatter(
 *   id = "elevenlabs_default",
 *   label = @Translation("Elevenlabs"),
 *   field_types = {"elevenlabs"}
 * )
 */
class ElevenLabsGenerationDefaultFormatter extends ElevenLabsFormatterBase {

  /**
   * The prefix to use for the global player.
   */
  protected static string $elevenLabsIdPrefix = 'elevenlabs-id-';

  /**
   * The prefix to use for items.
   */
  protected static string $elevenLabsItemIdPrefix = 'elevenlabs-audio-';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'show_speaker_label' => FALSE,
      'show_speaker_text' => FALSE,
      'virtual_player_enabled' => TRUE,
      'play_button_type' => 'hide',
      'global_play_button_text' => 'Play',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $element['virtual_player_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Virtual Player Enabled'),
      '#default_value' => $settings['virtual_player_enabled'],
    ];

    $element['show_speaker_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Speaker Label'),
      '#default_value' => $settings['show_speaker_label'],
    ];

    $element['show_speaker_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Speaker Text'),
      '#default_value' => $settings['show_speaker_text'],
    ];

    $element['play_button_type'] = [
      '#type' => 'select',
      '#options' => [
        'audio' => $this->t('Normal Audio Element'),
        'hide' => $this->t('Hide'),
      ],
      '#title' => $this->t('Item Audio Field'),
      '#default_value' => $settings['play_button_type'],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary[] = $this->t("Virtual Player: @virtual_player_enabled<br>
    Show Speaker Label: @show_speaker_label<br>
    Show Speaker Text: @show_speaker_text<br>
    Play Button Type: @play_button_type", [
      '@virtual_player_enabled' => $settings['virtual_player_enabled'] ? 'true' : 'false',
      '@show_speaker_label' => $settings['show_speaker_label'] ? 'true' : 'false',
      '@show_speaker_text' => $settings['show_speaker_text'] ? 'true' : 'false',
      '@play_button_type' => $settings['play_button_type'],
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $id = self::$elevenLabsIdPrefix . str_replace('_', '-', $this->fieldDefinition->getName());
    $renderedItems = [];
    $drupalSettings = [
      'currentTime' => 0,
      'state' => 'loading',
      'totalItems' => count($items),
    ];
    foreach ($items as $item) {
      // Render the items.
      $renderedItem = $this->preprocessItem($item);
      if ($renderedItem) {
        $renderedItems[] = $renderedItem;
        $drupalSettings['items'][] = $this->createDrupalSettings($item);
      }
    }
    // Should not happen.
    if (!count($renderedItems)) {
      return [];
    }
    // Render the whole entity.
    $elements['#attached'] = [
      'library' => [
        'elevenlabs_field/elevenlabs',
      ],
      'drupalSettings' => [
        'elevenlabs' => [
          $id => $drupalSettings,
        ],
      ],
    ];
    $elements['#theme'] = 'elevenlabs_field';
    $elements['#speeches'] = $renderedItems;
    $elements['#attributes'] = [
      'class' => [
        'elevenlabs-global-play',
      ],
      'id' => [
        $id,
      ],
      'disabled' => 'disabled',
    ];
    $elements['#global_play_button_text'] = $this->settings['global_play_button_text'];
    $elements['#global_play_button_enabled'] = FALSE;
    $elements['#virtual_player_enabled'] = $this->settings['virtual_player_enabled'];
    return $elements;
  }

  /**
   * Add to the setting for the virtual player.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item interface.
   *
   * @return array
   *   A part of the settings array.
   */
  private function createDrupalSettings(FieldItemInterface $item) {
    return [
      'id' => self::$elevenLabsItemIdPrefix . $item->history_item_id,
      'timePadding' => $item->start_time,
    ];
  }

  /**
   * Preprocess each item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item interface.
   *
   * @return array
   *   A render object.
   */
  private function preprocessItem(FieldItemInterface $item) {
    $speakers = $this->getSpeakers();
    // Should not really happen.
    if (!$item->target_id) {
      return FALSE;
    }
    $file = File::load($item->target_id);
    return [
      '#theme' => 'elevenlabs_audio',
      '#file_url' => $file ? \Drupal::service('file_url_generator')->generate($file->getFileUri())->toString() : 'not found',
      '#text' => $item->text,
      '#voice' => $speakers[$item->speaker] ?? $item->speaker,
      '#classes' => '',
      '#show_voice' => TRUE,
      '#show_text' => TRUE,
      '#play_button_type' => $this->settings['play_button_type'] ?? 'audio',
      '#show_speaker_label' => $this->settings['show_speaker_label'] ?? FALSE,
      '#show_speaker_text' => $this->settings['show_speaker_text'] ?? FALSE,
      '#attributes' => [
        'id' => self::$elevenLabsItemIdPrefix . $item->history_item_id,
      ],
    ];
  }

}
