<?php

namespace Drupal\elevenlabs_field\Plugin\Field\FieldType;

use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;

/**
 * Represents a configurable elevenlabs file field.
 */
class ElevenLabsFieldItemList extends FileFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    return [];
  }

}
