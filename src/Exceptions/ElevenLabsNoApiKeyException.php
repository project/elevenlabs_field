<?php

namespace Drupal\elevenlabs_field\Exceptions;

/**
 * No API key exception.
 */
class ElevenLabsNoApiKeyException extends \Exception {}
