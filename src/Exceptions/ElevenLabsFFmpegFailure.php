<?php

namespace Drupal\elevenlabs_field\Exceptions;

/**
 * FFmpeg command failed.
 */
class ElevenLabsFFmpegFailure extends \Exception {
}
