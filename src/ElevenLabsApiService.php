<?php

namespace Drupal\elevenlabs_field;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\elevenlabs_field\Exceptions\ElevenLabsNoApiKeyException;
use Drupal\elevenlabs_field\Form\ElevenLabsSettingsForm;
use GuzzleHttp\Client;

/**
 * Takes care of the API calls to ElevenLabs.
 */
class ElevenLabsApiService {

  /**
   * The API base path.
   */
  protected string $basePath = 'https://api.elevenlabs.io/v1/';

  /**
   * The default model.
   */
  public static string $defaultModel = 'eleven_multilingual_v2';

  /**
   * The config for ElevenLabs.
   */
  protected ImmutableConfig $config;

  /**
   * The guzzle client.
   */
  protected Client $client;

  /**
   * Construtor of ElevenLabs API Service.
   *
   * @param Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param GuzzleHttp\Client $client
   *   The Guzzle client.
   */
  public function __construct(ConfigFactory $configFactory, Client $client) {
    $this->config = $configFactory->get(ElevenLabsSettingsForm::CONFIG_NAME);
    $this->client = $client;
  }

  /**
   * Is the API setup and working.
   *
   * @return bool
   *   Is it working or not.
   */
  public function isSetup() {
    try {
      $this->getUserInfo();
      return TRUE;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Get voices.
   *
   * @return array
   *   An array of voices.
   */
  public function getVoices(): array {
    return json_decode($this->call('voices'), TRUE);
  }

  /**
   * Get models.
   *
   * @return array
   *   An array of models.
   */
  public function getModels(): array {
    return json_decode($this->call('models'), TRUE);
  }

  /**
   * Get user info.
   *
   * @return array
   *   The user info.
   */
  public function getUserInfo(): array {
    return json_decode($this->call('user'), TRUE);
  }

  /**
   * Generate voice.
   *
   * @param string $text
   *   Text.
   * @param string $voiceId
   *   The voice id.
   * @param string $modelId
   *   The model id.
   * @param array $options
   *   Extra options to send.
   *
   * @return string
   *   The binary.
   */
  public function textToSpeech($text, $voiceId, $modelId = '', array $options = []) {
    $modelId = $modelId ?? self::$defaultModel;
    $options['stability'] = $options['stability'] ?? 0;
    $options['similarity_boost'] = $options['similarity_boost'] ?? 0;
    $options['style'] = $options['style'] ?? 0.5;
    $options['use_speaker_boost'] = $options['use_speaker_boost'] ?? TRUE;
    $payload = [
      'text' => $text,
      'model_id' => $modelId,
      'voice_settings' => $options,
    ];
    return $this->call('text-to-speech/' . $voiceId, 'POST', $payload);
  }

  /**
   * Get history listing.
   *
   * @param int $pageSize
   *   How many objects to get.
   *
   * @return array
   *   The history.
   */
  public function getHistoryListing($pageSize = 10) {
    return json_decode($this->call('history', 'GET', [], ['page_size' => $pageSize]), TRUE);
  }

  /**
   * Make the API call.
   *
   * @param string $endpoint
   *   The endpoint to use.
   * @param string $method
   *   The method to use.
   * @param array $payload
   *   Any type of payload to attach.
   * @param array $queryString
   *   Any extra query string objects.
   * @param array $options
   *   Any extra Guzzle options.
   *
   * @return string
   *   The response unencoded.
   */
  private function call(string $endpoint, string $method = "GET", array $payload = [], array $queryString = [], array $options = []): string {
    if (empty($this->config->get('api_key'))) {
      throw new ElevenLabsNoApiKeyException("No API Key set.");
    }

    // Set initial options.
    $guzzleOptions = [
      'connect_timeout' => 5,
      'timeout' => 120,
      'headers' => [
        'content-type' => 'application/json',
        'xi-api-key' => $this->config->get('api_key'),
      ],
    ];

    // Overwrite if needed.
    $guzzleOptions = array_merge_recursive($options, $guzzleOptions);

    // Set payload if needed.
    if (!empty($payload)) {
      $guzzleOptions['json'] = $payload;
    }

    $url = $this->basePath . $endpoint;
    if (count($queryString)) {
      $url .= '?' . http_build_query($queryString);
    }

    $res = $this->client->request($method, $url, $guzzleOptions);

    return $res->getBody();
  }

}
