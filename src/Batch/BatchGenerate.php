<?php

namespace Drupal\elevenlabs_field\Batch;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Batch Job.
 */
class BatchGenerate {

  /**
   * {@inheritdoc}
   */
  public static function setBatchJob(ContentEntityInterface $entity, $field) {
    $operations = [];
    foreach ($entity->{$field} as $key => $value) {

      $operations[] = [
        '\Drupal\elevenlabs_field\Batch\BatchGenerate::generateItem', [
          [
            'entity' => $entity,
            'item' => $value,
            'field' => $field,
            'key' => $key,
          ],
        ],
      ];
    }
    $batch = [
      'title' => 'Creating Audio',
      'init_message' => 'Generating Audios',
      'operations' => $operations,
      'finished' => '\Drupal\elevenlabs_field\Batch\BatchGenerate::finishedBatching',
    ];
    batch_set($batch);
  }

  /**
   * Batch function.
   */
  public static function generateItem($item, &$context) {
    $eItem = $item['item'];
    // If already set.
    if ($eItem->target_id || !$eItem->text) {
      return;
    }
    $generator = \Drupal::service('elevenlabs_field.generator_service');
    $model = $generator->validateModel($eItem->model_id, $eItem->getEntity());
    $data = $generator->generateFile($eItem->text, $eItem->speaker, $eItem->getFieldDefinition(), $model, [
      'stability' => ($eItem->stability / 100),
      'similarity_boost' => ($eItem->similarity_boost / 100),
      'style' => ($eItem->style_exaggeration / 100),
      'use_speaker_boost' => $eItem->speaker_boost ? TRUE : FALSE,
    ]);
    if (!empty($data['file'])) {
      // Remove old data if exists.
      if ($eItem->target_id) {
        $generator->removeFile($eItem->target_id);
      }
      $eItem->entity = $data['file'];
      $eItem->set('target_id', $data['file']->id());
      $eItem->set('history_item_id', $data['history_item_id']);
    }
    // Resave them.
    if (!isset($context['results']['entity'])) {
      $context['results']['entity'] = $item['entity'];
    }
    $context['results']['entity']->{$item['field']}[$item['key']] = $eItem;

  }

  /**
   * Finished function.
   */
  public static function finishedBatching($success, $results, $operations) {
    if (isset($results['entity'])) {
      $results['entity']->save();
    }
  }

}
