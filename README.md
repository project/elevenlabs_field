# ElevenLabs Field

ElevenLabs field lets you add a complex field to entities that can generate
audio using the ElevenLabs API service. This can be use for accessability
purposes or even to create virtual podcasts.

The module comes with a "Virtual Player", that can play multiple audios in a set
order, but also possibility to copy the fields values to normal File fields or
Media entities and even to concatenate a whole dialogue into one mp3.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

This module requires an ElevenLabs API account:

- [ElevenLabs](https://elevenlabs.io/)

## Recommended extras

- [FFMpeg](https://ffmpeg.org/): Makes it possible to mix dialogue into one mp3 file.

## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
- Visit /admin/config/system/eleven-labs-settings to setup the API key.
- On your node, block, taxonomy or other entity, just add an ElevenLabs Field.

## Configuration

Configure the admin toolbar tools at (/admin/config/system/eleven-labs-settings).

## Maintainers

Current maintainers:

- [Marcus Johansson (marcus_johansson)](https://www.drupal.org/u/marcus_johansson)
