<?php

namespace Drupal\elevenlabs_bytescale\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure BytescaleForm API access.
 */
class BytescaleForm extends ConfigFormBase {

  /**
   * Config settings.
   */
  const CONFIG_NAME = 'elevenlabs_bytescale.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'elevenlabs_bytescale_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bytescale Secret Account ID'),
      '#description' => $this->t('Can be found and generated <a href="https://www.bytescale.com/dashboard/files" target="_blank">here</a>.'),
      '#default_value' => $config->get('account_id'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bytescale Secret API Key'),
      '#description' => $this->t('Can be found and generated <a href="https://www.bytescale.com/dashboard/files" target="_blank">here</a>.'),
      '#default_value' => $config->get('api_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('account_id', $form_state->getValue('account_id'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
