<?php

namespace Drupal\ai_interpolator_elevenlabs_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'list_elevenlabs_voices_widget' widget.
 *
 * @FieldWidget(
 *   id = "list_elevenlabs_voices_widget",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "elevenlabs_voices"
 *   },
 *   multiple_values = TRUE
 * )
 */
class ElevenLabsVoicesWidget extends OptionsSelectWidget {

}
