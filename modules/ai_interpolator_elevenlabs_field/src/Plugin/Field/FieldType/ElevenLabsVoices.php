<?php

namespace Drupal\ai_interpolator_elevenlabs_field\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;

/**
 * Plugin implementation of the 'elevenlabs_voices' field type.
 *
 * @FieldType(
 *   id = "elevenlabs_voices",
 *   label = @Translation("ElevenLabs Voices"),
 *   description = @Translation("A field type that provides the list of the voices to use with AI Interpolator."),
 *   category = @Translation("Custom"),
 *   default_widget = "list_elevenlabs_voices_widget",
 *   default_formatter = "list_elevenlabs_voices"
 * )
 */
class ElevenLabsVoices extends ListStringItem {

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    $voices = \Drupal::service('elevenlabs_field.api_service')->getVoices();
    $options = [];
    if (!empty($voices['voices'])) {
      foreach ($voices['voices'] as $voice) {
        $options[$voice['voice_id']] = $voice['name'];
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];
    return $element;
  }

}
