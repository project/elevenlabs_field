<?php

namespace Drupal\ai_interpolator_elevenlabs_field\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'list_elevenlabs_voices' formatter.
 *
 * @FieldFormatter(
 *   id = "list_elevenlabs_voices",
 *   label = @Translation("Voices"),
 *   field_types = {
 *     "elevenlabs_voices",
 *   }
 * )
 */
class ElevenLabsVoicesFormatter extends OptionsDefaultFormatter {

}
