<?php

/**
 * @file
 * Provides a field that can create audio.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\elevenlabs_field\Batch\BatchGenerate;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function elevenlabs_field_form_field_ui_field_storage_add_form_alter(&$form, FormStateInterface $formState) {
  // Don't allow generation of Elevenlabs fields before it is setup.
  if (!\Drupal::service('elevenlabs_field.api_service')->isSetup()) {
    $form['#validate'][] = 'elevenlabs_field_form_validation_elevenlabs_field_not_setup';
  }
}

/**
 * Implements hook_entity_presave().
 */
function elevenlabs_field_entity_presave(EntityInterface $entity) {
  $current_batch = batch_get();
  // Get queue and see if it's there.
  $entities = _elevenlabs_field_needs_copying();
  if (isset($entities[$entity->getEntityTypeId() . $entity->id()])) {
    $rules = $entities[$entity->getEntityTypeId() . $entity->id()];
    $values = $entity->get($rules['original_field'])->getValue();
    // If we should batch.
    if (!empty($values) && !empty($rules['batch']) && (!isset($current_batch['sets'][0]['title']) || $current_batch['sets'][0]['title'] != 'Creating Audio')) {
      BatchGenerate::setBatchJob($entity, $rules['original_field']);
    }
    switch ($rules['type']) {
      case 'concatenate':
        // Only change if something changed or field is empty.
        if (empty($entity->get($rules['field'])->getValue()) || (isset($entity->original) &&
          json_encode($entity->get($rules['original_field'])->getValue()) != json_encode($entity->original->get($rules['original_field'])->getValue()))) {
          $entity->set($rules['field'], \Drupal::service('elevenlabs_field.ffmpeg_service')->concanateAudios($values, $rules['original_field'], $rules['field'], $entity));
        }
        break;
    }
  }
}

/**
 * Implements hook_theme().
 */
function elevenlabs_field_theme($existing, $type, $theme, $path) {
  return [
    'elevenlabs_field' => [
      'variables' => [
        'attributes' => [],
        'global_play_button_enabled' => TRUE,
        'virtual_player_enabled' => TRUE,
        'global_play_button_text' => 'Play',
        'speeches' => 'test',
        'speech_list_enabled' => TRUE,
        'items' => [],
        'label_display' => '',
        'label' => '',
      ],
    ],
    'elevenlabs_audio' => [
      'variables' => [
        'attributes' => '',
        'file_url' => '',
        'voice' => '',
        'text' => '',
        'show_speaker_label' => '',
        'show_speaker_text' => '',
        'play_button_type' => '',
      ],
    ],
  ];
}

/**
 * Notice that Elevenlabs has to be setup.
 */
function elevenlabs_field_form_validation_elevenlabs_field_not_setup(&$form, FormStateInterface $formState) {
  if ($formState->getValue('new_storage_type') == 'elevenlabs') {
    $formState->setErrorByName('new_storage_type', 'You have to setup ElevenLabs first. Please refer to documentation of the module.');
  }
}

/**
 * Static that keeps track of any audio copying needed.
 */
function _elevenlabs_field_needs_copying($entity = NULL, $rules = []) {
  static $entities;
  if ($entity) {
    $entities[$entity->getEntityTypeId() . $entity->id()] = $rules;
  }
  return $entities;
}
